# README

## About

**Fido** is a light-weight cached disk usage estimator monitor.
Like an eager watch dog **fido** will warn you, for example by email, if the disk usage exceeds a certain threshold.

## Install

### Install with pip

To install **fido** with [pip](https://en.wikipedia.org/wiki/Pip_(package_manager)), simply execute
```
$ pip install git+https://bitbucket.org/csadorf/fido.git#egg=fido --user
```
To update your installation, add the `--upgrade` option to the above command.

### By cloning the git repository

Clone the repository and then execute
```
$ cd fido
$ python setup.py install --user
```

### Adjusting PATH

If the **fido** executable is not found after install make sure that your local binary path, usualy `~/.local/bin` is configured in your `PATH` environment variable.

## Quickstart

To watch a directory execute the following command
```
#!text
$ fido /watched/path --human --threshold=130GB
Hi, I'm Fido!
I'm watching '/watched/path' for disk usage.
I'm estimating the size every 5 minutes.
This is what I'll do if the estimated size exceeds 130.0GiB:
  - I'll warn you here.

Mon Jun 15 12:00:00 2015 118.6GiB /watched/path
Usage: 91%.
Estimated disk growth: 5.0MiB/s
Exceeding threshold in about 39 minutes.
```

To minimize the server load, **fido** caches all filesystem queries.
**This means you need to tell fido where to expect changes!**

## Details

An invocation of the `fido` executable uses `duc` (`du` with cached results, described below) to monitor a specific directory and its subdirectories.
**Fido** can be run interactively or as a daemon and compares the results of periodic calls to `duc`.
Depending on command line options, **fido** can force **duc** to expire its cache immediately, according to regular expressions, and/or by checking a *dirty path* list in the watched directory.
Sub-directories of the watched path can be marked with `fidomark` such that the cache is refreshed for the next call to `duc` or for all future calls to `duc`.
For a subdirectory or set of files that you know is growing, such as while a job is running, you must make sure that fido always gets fresh data.
When the job is done and for directories that you know are not of interest, you should try to make sure duc is giving **fido** cached data.

### Using regular expressions

You can either provide a *regex* expression, such as `my_jobs_*_look_like_this`:
```
$ fido /watched/path -u "my_jobs_*_look_like_this"
```
**Fido** will then only query the filesystem for paths that match this expression.

### Marking directories as active

Or you can mark certain directories as *active*, for example by adding the following commands to your job scripts, using bash:
```
#!bash
$ fidomark /watched/path .  # at the start
# [...]
$ fidomark /watched/path . --once # at the end
```
Or python:
```
#!python
import fido, os
fido.mark('/watched/path', os.getcwd()) # at the start
#[...]
fido.mark('/watched/path', os.getcwd(), once = True) # at the end
```
The option *once* instructs **fido** to delete this mark after the next cache update.

## Automated alerts

To execute certain actions and/or send an email upon reaching a specific threshold, add the following options
```
$ fido /watched/path -e johndoe@umich.edu -a "touch /watched/path/STOP_FLAG"
```
If you want to be alerted by email, you will need to provide your UMich lvl-1 password after start.

## Run in background

To run **fido** as daemon, use the `--daemon` or `-d` argument:
```
#!text
$ fido /watched/path -e johndoe@umich.edu -t 200MB -l ~/fido.log --daemon
Hi, I am Fido!
I'm watching '/watched/path' for disk usage.
I'm estimating the size every 5 minutes.
This is what I'll do if the estimated size exceeds 200.0MiB:
  - I'll warn you here.
  - I'll send an email to johndoe@umich.edu
Logging to '/home/johndoe/fido.log'.

Starting daemon.
$ 
```
To stop the daemon, simply execute
```
$ fido /watched/path --stop
Stopping daemon.
```
You can run one daemon per watched directory. If you want to **stop all daemons**, use
```
$ fido /some/path --stop-all
Stopping all daemons.
```

## du-cached

To simply get cached directory size estimates, use **duc**, which is part of this package. **duc** is a cached clone of the unix command line tool *du*.
```
$ duc /a/path
/a/path/abc.txt 12394
/a/path/video.mpeg 3980238
3992632
```
Both **duc** and **fido** operate on the same cache.

## In combination with signac

When [signac](https://bitbucket.org/glotzer/signac) is installed, **fido** can determine active paths automatically:
```
#!text
$ fido /watched/path --signac
Hi, I am Fido!
I'm watching '/watched/path' for disk usage.
I expect changes on paths matching open signac job ids.
I'm estimating the size every 5 minutes.

Mon Jun 15 12:00:00 2015 191.1MiB /watched/path
```
This option provides the *job id* of every open job as update expression.