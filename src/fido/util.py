def add_verbosity_argument(parser, default = 0):
    """Add a verbosity argument to parser.

    Note:
      The argument is '-v' or '--verbosity'.
      Add multiple '-v' arguments, e.g. '-vv' or '-vvv' to 
      increase the level of verbosity.

    Args:
      parser: A argparse object.
      default: The default level, defaults to 0.
    """
    parser.add_argument(
        '-v', '--verbosity',
        help = "Set level of verbosity.",
        action = 'count',
        default = default,
        )

def set_verbosity_level(verbosity, default = None, increment = 10):
    """Set the verbosity level as a function of an integer level.

    Args:
      verbosity: The verbosity level as integer.
      default: The default verbosity level, defaults to logging.ERROR.
    """
    import logging
    if default is None:
        default = logging.ERROR
    logging.basicConfig(
        level = default - increment * verbosity)

