#!/usr/bin/env python3

import os
import io
import sys
import logging
import re
import getpass
import subprocess
from email.mime.text import MIMEText

from .duc import clear_history, get_history, estimate_total_usage, sizeof_fmt
from . import sendmail as sm

logger = logging.getLogger(__name__)

FN_PID = os.path.expanduser('~/.fido.{rid}.pid')

RE_EMAIL = r"[^@]+@[^@]+\.[^@]+"
EMAIL_USERNAME = getpass.getuser()
EMAIL_FROM = EMAIL_USERNAME + '@umich.edu'
EMAIL_HOST = 'smtp.mail.umich.edu'
EMAIL_PORT = 587
EMAIL_BODY = """
    WARNING
    
    You're threshold of {threshold} for {directory} is exceeded by {ratio:.0f}%.

    Currently at: {totalsize}
    ({timestamp})
    """
EMAIL_DEFAULT_TIMEOUT=10

MSG_WELCOME = """Hi, I am fido!
I'm watching '{directory}' for disk usage."""

def fn_pid(args):
    import hashlib
    m = hashlib.md5()
    m.update(args.directory.encode())
    return FN_PID.format(rid = m.hexdigest())

def all_fn_pids():
    import glob
    yield from glob.glob(FN_PID.format(rid = '*'))

def prompt_pwd(msg = None):
    import getpass
    if msg is None:
        msg = "Please enter your password: "
    return getpass.getpass(msg)

import base64
def encode_pwd(pwd):
    return base64.standard_b64encode(pwd.encode()).decode()

def decode_pwd(pwd):
    return base64.standard_b64decode(pwd.encode()).decode()

def format_total_size(dirname, value, ts = None, human = False):
    import time
    if ts is None:
        ts = time.time()
    s = "{time} {value} {dirname}"
    if human:
        return s.format(
            dirname=dirname,
            time = time.ctime(float(ts)),
            value=sizeof_fmt(value))
    else:
        return s.format(
            dirname=dirname,
            time = time.ctime(float(ts)),
            value=value)

def format_time(timedelta):
    if timedelta > 3600 * 24:
        return "{:.0f} days".format(timedelta / 3600 / 24)
    elif timedelta > 3600:
        return "{:.0f} hours".format(timedelta/ 3600)
    elif timedelta > 60:
        return "{:.0f} minutes".format(timedelta / 60)
    else:
        return "{} seconds".format(timedelta)

def welcome_message(args, pipe=sys.stdout):
    pipe.write(MSG_WELCOME.format(directory = args.directory)+'\n')
    if args.update:
        if args.update_cmd:
            pipe.write("I expect updates on paths matching the pattern given by '{}'.\n".format(args.update))
        else:
            pipe.write("I expect updates on paths matching the pattern '{}'.\n".format(args.update))
    try:
        if args.signac:
            pipe.write("I expect changes on paths matching open signac job ids.\n")
    except AttributeError:
        pass
    if args.interval:
        pipe.write("I'm estimating the size every {}.\n".format(format_time(args.interval)))
    if args.threshold:
        pipe.write("This is what I'll do if the estimated size exceeds {}:\n".format(sizeof_fmt(args.threshold)))
        pipe.write("  - I'll warn you here.\n")
        if args.action:
            pipe.write("  - I'll execute '{}'.\n".format(args.action))
        if args.email:
            pipe.write("  - I'll send an email to {}.\n".format(args.email))
    if args.logfile:
        pipe.write("Logging to '{}'.\n".format(args.logfile))
#    if args.daemon:
#        pipe.write("Fido will run as daemon in the background!")
    pipe.write('\n')

def execute_action(args, totalsize, ratio):
    import time
    if args.email:
        msg = MIMEText(EMAIL_BODY.format(
            directory = args.directory,
            threshold=sizeof_fmt(args.threshold),
            totalsize=sizeof_fmt(totalsize),
            ratio=ratio*100,
            timestamp = time.ctime()))
        msg['Subject'] = "[fido] WARNING: Exceeding threshold for {}!".format(args.directory)
        msg['From'] = EMAIL_FROM
        msg['To'] = args.email
        sendmail(args, msg)
        args.email = None # Only send once!
    if args.action:
        import subprocess
        subprocess.call(args.action.split())

def check_threshold(args, total_size, fd = None):
    import sys
    if fd is None:
        fd = sys.stdout
    ratio = total_size / args.threshold
    msg = "Usage: {r:.0f}% of {t}."
    if ratio >= 1.0:
        msg += " Exceeding threshold!"
        execute_action(args, total_size, ratio)
    if args.human:
        print(msg.format(r=ratio * 100, t=sizeof_fmt(args.threshold)),file=fd)
    else:
        print(msg.format(r=ratio * 100, t=args.threshold),file=fd)
    history = get_history(args)
    if history is not None:
        timestamps = list(sorted(history.keys()))
        if len(timestamps) >= 2:
            t0, t1 = timestamps[-2], timestamps[-1]
            u0, u1 = int(history[t0]), int(history[t1])
            dudt = (u1-u0)/(float(t1) - float(t0))
            print("Estimated disk growth: {}/s".format(sizeof_fmt(dudt)),file=fd)
            if dudt > 0:
                delta_u = args.threshold - u1
                if delta_u > 0:
                    delta_t = delta_u / dudt
                    print("Exceeding threshold in about {}.".format(format_time(delta_t)),file=fd)
    return ratio >= 1

class SendMailError(RuntimeError):
    pass

from .daemon import Daemon
class FidoDaemon(Daemon):

    def run(self):
        import time
        args = self.args
        while(True):
            total_size = estimate_total_usage(args)
            if args.logfile:
                with open(args.logfile, 'a') as fd:
                    print(format_total_size(args.directory, total_size, human=args.human), file=fd)
                    if args.threshold:
                        check_threshold(args, total_size, fd=fd)
            time.sleep(args.interval)

def init(args):
    if args.interval and args.human:
        welcome_message(args)
    if args.clear_history:
        try:
            clear_history(args)
        except FileNotFoundError:
            pass
    if args.show_history:
        history = get_history(args)
        if history:
            for ts in sorted(history.keys()):
                print(format_total_size(args.directory, history[ts], ts, args.human))

def run_single(args):
    init(args)
    if args.interval:
        def run_periodic():
            from threading import Timer
            total_size = estimate_total_usage(args)
            if args.logfile:
                with open(args.logfile, 'a') as fd:
                    print(format_total_size(args.directory, total_size, human=args.human), file=fd)
                    if args.threshold:
                        check_threshold(args, total_size, fd=fd)
            else:
                print(format_total_size(args.directory, total_size, human=args.human))
                if args.threshold:
                    check_threshold(args, total_size)
            Timer(args.interval, run_periodic).start()
        run_periodic()
    else:
        total_size = estimate_total_usage(args)
        if args.logfile:
            with open(args.logfile, 'a') as fd:
                print(format_total_size(args.directory, total_size, human=args.human), fd=fd)
                if args.threshold:
                    check_threshold(args, total_size, fd=fd)
        else:
            print(format_total_size(args.directory, total_size, human=args.human))
            if args.threshold:
                check_threshold(args, total_size)
    return 0

def run(args):
    import time
    if args.stop_all:
        print("Stopping all daemons.")
        for fn_pid_ in all_fn_pids():
            daemon = FidoDaemon(fn_pid_)
            try:
                daemon.stop()
            except:
                raise
        return 0
    daemon = FidoDaemon(fn_pid(args))
    if args.stop:
        print("Stopping daemon.")
        daemon.stop()
        return 0
    if args.daemon:
        init(args)
        print("Starting daemon.")
        daemon.args = args
        daemon.start()
    else:
        return run_single(args)

def sendmail(args, msg):
    if args.smtp:
        try:
            print("sending via smtp")
            sm.sendmail_smtp(EMAIL_HOST, EMAIL_PORT, EMAIL_USERNAME, decode_pwd(args.password), msg=msg)
            print("done")
        except Exception:
            raise SendMailError() from error
        else:
            print('success')
    else:
        try:
            sm.sendmail(msg, timeout=args.email_timeout)
        except subprocess.TimeoutExpired as error:
            raise SendMailError('Timeout expired.') from error
        except FileNotFoundError as error:
            raise SendMailError("Unable to find 'sendmail'.") from error

def main(arguments = None):
    import sys, argparse
    from .duc import setup_parser, os
    from .util import add_verbosity_argument, set_verbosity_level
    parser = argparse.ArgumentParser(
        description = "A light-weight cached disk usage monitoring tool.")
    setup_parser(parser)
    parser.add_argument(
        '--show-history',
        action = 'store_true',
        help = "Show the cached history.")
    parser.add_argument(
        '--clear-history',
        action = 'store_true',
        help = "Clear the total size history.")
    parser.add_argument(
        '-t', '--threshold',
        type = str,
        help = "The lower threshold for the alarm (in bytes).")
    parser.add_argument(
        '-i', '--interval',
        type = int,
        default = 300,
        help = "The interval in which to check the threshold in seconds.")
    parser.add_argument(
        '-l', '--logfile',
        type = str,
        help = "Write output to logfile.")
    parser.add_argument(
        '-e', '--email',
        type = str,
        help = "An email address to send an email to once the threshold is reached.")
    parser.add_argument(
        '--smtp',
        action = 'store_true',
        help = "Use this option to send e-mails via a smtp server instead of 'sendmail'.")
    parser.add_argument(
        '--test-email',
        action = 'store_true',
        help = "Send an email after starting the process.")
    parser.add_argument(
        '--email-timeout',
        type = int,
        default = EMAIL_DEFAULT_TIMEOUT,
        help = "Time in seconds before timeout when trying to send mail.")
    parser.add_argument(
        '-a', '--action',
        type = str,
        help = "A command to execute once threshold is reached.")
    parser.add_argument(
        '-d', '--daemon',
        action = 'store_true',
        help = "Run fido as daemon.")
    parser.add_argument(
        '--stop',
        action = 'store_true',
        help = "Stop the fido daemon.")
    parser.add_argument(
        '--stop-all',
        action = 'store_true',
        help = "Stop all fido daemons.")
    add_verbosity_argument(parser)
    args = parser.parse_args(arguments)
    args.summarize = True
    args.directory = os.path.abspath(args.directory)
    if args.logfile:
        args.logfile = os.path.abspath(args.logfile)
    if args.threshold:
        from .duc import parse_sizestr
        args.threshold = parse_sizestr(args.threshold)
    set_verbosity_level(args.verbosity)
    try:
        if args.email:
            if not re.match(RE_EMAIL, args.email):
                msg = "Not a valid email address: '{}'."
                raise ValueError(msg.format(args.email))
            if args.smtp:
                args.password = encode_pwd(prompt_pwd("Enter your smtp server password:"))
                sendmail(args, None) # try to connect to server
            if args.test_email:
                pipe = io.StringIO()
                welcome_message(args, pipe)
                msg = MIMEText(pipe.getvalue())
                msg['Subject'] = "[fido] Watching {}".format(args.directory)
                msg['From'] = EMAIL_FROM
                msg['To'] = args.email
                print("Sending test email.")
                sendmail(args, msg)
        return run(args)
    except SendMailError as error:
        print("Error while trying to send mail: {}".format(error))
        if not args.smtp:
            print("Consider to use option '--smtp'.")
        return 1
    except Exception as error:
        if args.verbosity > 1:
            raise
        else:
            print("Error: {}".format(error))
            return 1

if __name__ == '__main__':
    import sys
    logging.basicConfig(level = logging.WARNING)
    try:
        sys.exit(main(args))
    except KeyboardInterrupt:
        print("Interrupted.")
        sys.exit(0)
