#!/usr/bin/env python

import logging
logger = logging.getLogger(__name__)

from . import mark

def main(arguments = None):
    import argparse
    parser = argparse.ArgumentParser(
        description = "This is a helper tool for fido. Mark a specified location dirty for fido.")
    parser.add_argument(
        'directory',
        type = str,
        help = "The directory which is watched.")
    parser.add_argument(
        'dirtypath',
        type = str,
        help = "The path to mark as dirty.")
    parser.add_argument(
        '-o', '--once',
        action = 'store_true',
        help = "Unmark after next cache update. Use this option if you don't expect any changes anymore.")
    args = parser.parse_args(arguments)
    logging.basicConfig(level = logging.WARNING)
    mark(args.directory, args.dirtypath, args.once)
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main())
