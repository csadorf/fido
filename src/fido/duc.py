#!/usr/bin/env python3

import logging
logger = logging.getLogger(__name__)

from . dirsize import get_size, update_cache, update_cache_match, clear_cache
from . import VERSION

import os
FN_CACHE_DEFAULT = os.path.expanduser('~/.cache/fido.json')

KEY_HISTORY = 'history'
KEY_VERSION = 'version'

PADDING_SIZE = 8
VERSION_TUPLE = 0,2

def get_fn_lock(fn):
    return fn + '.lock'

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def parse_sizestr(size_str, suffix = ''):
    size_str = size_str.strip().lower()
    for i, unit in enumerate(['k','m','g','t','p','e','z']):
        r = size_str.rfind(unit)
        if r > 0:
            return int(size_str[:r]) * pow(1024, i+1)
    return int(size_str)

def calc_total_size(root, cache = None,
                    dump = False, human = False):
    if dump:
        sum_ = 0
        for fp, s in get_size(root, cache=cache):
            sum_ += s
            if human:
                print(sizeof_fmt(s).ljust(PADDING_SIZE), fp)
            else:
                print(str(s).ljust(PADDING_SIZE), fp)
        return sum_
    else:
        return sum((v[1] for v in get_size(root, cache=cache)))

def get_history(args):
    import json
    if args.cache != '':
        try:
            with open(args.cache, 'rb') as cachefile:
                cache = json.loads(cachefile.read().decode())
                history = cache.get(KEY_HISTORY)
                if history:
                    return history.get(args.directory)
        except FileNotFoundError:
            return None

def clear_history(args):
    import json, fcntl
    if args.cache != '':
        with open(get_fn_lock(args.cache), 'w') as lockfile:
            fcntl.lockf(lockfile, fcntl.LOCK_EX)
            with open(args.cache, 'rb') as cachefile:
                cache = json.loads(cachefile.read().decode())
                try:
                    del cache[KEY_HISTORY]
                except KeyError:
                    pass
            with open(args.cache, 'wb') as cachefile:
                cachefile.write(json.dumps(cache).encode())
                cachefile.write('\n'.encode())

def update_flagged(cache, root):
    from . import fn_flag, _mark
    with open(fn_flag(root), 'rb') as flagfile:
        flags = flagfile.read().decode()
    to_keep = set()
    to_delete = set()
    for line in flags.split('\n'):
        if line:
            fn, delete = line.split()
            fn = os.path.join(root, fn)
            if fn in to_keep or fn in to_delete:
                continue
            logger.info("Flagged: {}".format(fn))
            if int(delete):
                to_delete.add(fn)
            else:
                to_keep.add(fn)
    to_update = to_keep.union(to_delete)
    update_cache(cache, root, to_update)
    to_keep = to_keep.difference(to_delete)
    if to_keep:
        with open(fn_flag(root), 'wb') as flagfile:
            for fn in to_keep:
                _mark(flagfile, root, fn)
    else:
        try:
            os.remove(fn_flag(root))
        except FileNotFoundError:
            pass

def update_with_signac(root, cache):
    import re
    from signac.contrib import get_all_active_jobs
    for job_id in get_all_active_jobs():
        logger.info("CompDB active job: {}".format(job_id))
        match = re.compile(job_id)
        update_cache_match(cache, root, match.search)

def _load_cache(fn_cache):
    import json
    with open(fn_cache, 'rb') as cachefile:
        return json.loads(cachefile.read().decode())

def write_cache(fn_cache, cache):
    logger.info("Writing cache file '{}'.".format(fn_cache))
    import json
    cache[KEY_VERSION] = VERSION_TUPLE
    with open(fn_cache, 'wb') as cachefile:
        cachefile.write(json.dumps(cache).encode())
        cachefile.write('\n'.encode())

def load_cache(fn_cache):
    import shutil
    logger.info("Reading cache file '{}'.".format(fn_cache))
    cache = dict()
    try:
        try:
            cache.update(_load_cache(fn_cache))
        except ValueError:
            msg = "Cache file seems corrupted. Loading backup."
            logger.warning(msg)
            shutil.copy(fn_cache + '~', fn_cache)
            cache.update(_load_cache(fn_cache))
        if VERSION_TUPLE > tuple(cache[KEY_VERSION]):
            raise KeyError()
    except FileNotFoundError as error:
        msg = "Did not find any cache file. Building cache."
        logger.warning(msg)
    except ValueError:
        msg = "Cache file seems corrupted. Rebuilding cache."
        logger.warning(msg)
    except KeyError:
        msg = "Cache file not compatible with this version. Rebuilding cache."
        logger.warning(msg)
    return cache

def update_cache_with_regex(cache, root, regex):
    import re
    logger.info("Regex pattern '{}'.".format(regex))
    to_update = re.compile(regex.strip())
    update_cache_match(cache, root, to_update.search)

def perform_cache_updates(cache, root, regex = None):
    if regex is not None:
        update_cache_with_regex(cache, root, regex)
    try:
        update_flagged(cache, root)
    except FileNotFoundError:
        pass

def run_estimate(root, fn_cache = None, regex = None, clear = False, dump = False, human = False, signac = False):
    import shutil, fcntl, time
    root = os.path.abspath(root)
    update = None
    if fn_cache is not None:
        cache = load_cache(fn_cache)
        if cache:
            with open(get_fn_lock(fn_cache), 'wb') as lockfile:
                fcntl.lockf(lockfile, fcntl.LOCK_EX)
                if clear:
                    clear_cache(cache, root)
                else:
                    perform_cache_updates(cache, root, regex)
            if signac:
                try:
                    update_with_signac(root, cache)
                except ImportError:
                    pass
    else:
        cache = None
    try:
        total_size = calc_total_size(
            root, cache, dump = dump, human = human)
        if cache:
            cache.setdefault(KEY_HISTORY, dict())
            cache[KEY_HISTORY].setdefault(root, dict())
            cache[KEY_HISTORY][root][time.time()] = total_size
    except KeyboardInterrupt:
        print()
        print("Interrupted!")
        raise
    else:
        return total_size
    finally:
        if cache:
            import shutil
            fn_cache_lock = get_fn_lock(fn_cache)
            dir_cache = os.path.dirname(fn_cache_lock)
            if not os.path.exists(dir_cache):
                os.makedirs(dir_cache)
            with open(fn_cache_lock, 'wb') as lockfile:
                fcntl.lockf(lockfile, fcntl.LOCK_EX)
                try:
                    cache_tmp = _load_cache(fn_cache)
                    logger.debug("Creating cache backup.")
                    shutil.copyfile(fn_cache, fn_cache + '~')
                except (FileNotFoundError, ValueError):
                    cache_tmp = dict()
                cache_tmp.update(cache)
                write_cache(fn_cache, cache_tmp)

def estimate_total_usage(args):
    if args.update_cmd:
        import subprocess
        regex = subprocess.check_output(cmd_regex.split()).decode()
    else:
        regex = args.update
    try:
        signac = args.signac
    except AttributeError:
        signac = False
    return run_estimate(
        root = args.directory,
        fn_cache = args.cache,
        regex = regex,
        clear = args.clear_cache,
        dump = not args.summarize,
        human = args.human,
        signac = signac)

def run(args):
    total_size = estimate_total_usage(args)
    if args.human:
        print(sizeof_fmt(total_size).ljust(PADDING_SIZE), args.directory)
    else:
        print(str(total_size).ljust(PADDING_SIZE), args.directory)
    return 0

def setup_parser(parser):
    parser.add_argument(
        'directory',
        type = str,
        help = "The directory to watch.")
    parser.add_argument(
        '-H', '--human',
        action = 'store_true',
        help = "Human readable output of sizes.")
    parser.add_argument(
        '-c', '--cache',
        type = str,
        default = FN_CACHE_DEFAULT,
        help = "The path to the cache file.")
    parser.add_argument(
        '--clear-cache',
        action = 'store_true',
        help = "Clear the cache of the selected path.")
    parser.add_argument(
        '-u', '--update',
        type = str,
        help = "A regex expression for all directories to update in cache.")
    parser.add_argument(
        '--update-cmd',
        action = 'store_true',
        help = "The update argument is interpreted as a command, where the output of this command is a regex pattern.")
    try:
        import signac
    except ImportError:
        pass
    else:
        parser.add_argument(
            '--signac',
            action = 'store_true',
            help = "Use signac to determine active paths.")
    parser.add_argument(
        '-V', '--version',
        action = 'version',
        version = '%(prog)s {}'.format(VERSION))

def main(arguments = None):
    import sys, argparse
    from .util import add_verbosity_argument, set_verbosity_level
    parser = argparse.ArgumentParser(
        description = "A light-weight cached disk usage estimator.")
    setup_parser(parser)
    parser.add_argument(
        '-s', '--summarize',
        action = 'store_true',
        help= 'Display only a total.')
    add_verbosity_argument(parser)   
    args = parser.parse_args(arguments)
    set_verbosity_level(args.verbosity)
    return run(args)

if __name__ == '__main__':
    import sys
    logging.basicConfig(level = logging.WARNING)
    sys.exit(main(args))
