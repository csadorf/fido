BIN_SENDMAIL='/usr/sbin/sendmail'

def sendmail(msg, timeout=None):
    import subprocess
    pipe = subprocess.Popen([BIN_SENDMAIL, '-t', '-oi'], stdin=subprocess.PIPE, universal_newlines=True)
    pipe.communicate(msg.as_string(), timeout=timeout)

def sendmail_smtp(host, port, username, pwd, msg, debug = False):
    import smtplib
    with smtplib.SMTP(host, port) as server:
        if debug:
            server.set_debuglevel(1)
        server.ehlo()
        server.starttls()
        server.login(username, pwd)
        if msg is not None:
            server.send_message(msg)
        server.quit()
