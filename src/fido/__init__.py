import os

FN_FLAG = '.fido_dirty_paths.txt'
VERSION_TUPLE = 0,2,2
VERSION = '.'.join(str(v) for v in VERSION_TUPLE)

def fn_flag(path):
    return os.path.join(path, FN_FLAG)

def _mark(flagfile, root, path, once = False):
    rp = os.path.relpath(path, root)
    f = str(1 if once else 0)
    l = rp + ' ' + f + '\n'
    flagfile.write(l.encode())
    flagfile.flush()

def mark(root, path, once = False):
    root = os.path.abspath(root)
    with open(fn_flag(root), 'ab') as flagfile:
        return _mark(flagfile, root, path, once=once)

def mark_multiple(root, paths, once = False):
    root = os.path.abspath(root)
    with open(fn_flag(root), 'ab') as flagfile:
        for path in paths:
            _mark(flagfile, root, path, once = once)
