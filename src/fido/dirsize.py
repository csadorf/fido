import logging
logger = logging.getLogger(__name__)

KEY_FLAGGED = 'flagged'
KEY_LS = 'ls'
KEY_SIZE = ':size'

import os

def issubdir(root, dirname):
    r = os.path.realpath(root)
    d = os.path.realpath(dirname)
    return d.startswith(r)

def _get_size(root = '.'):
    for dirpath, dirnames, filenames in os.walk(root):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            try:
                s = os.path.getsize(fp)
                yield fp, s
            except OSError as error:
                msg = "{}"
                logger.warning(msg.format(error))

def _get_nested(mapping, keys, default = dict):
    for key in keys:
        mapping = mapping.setdefault(key, default())
    return mapping

def _walk(node, root = None):
    from collections.abc import Mapping
    if root is None:
        root = ''
    for key, item in node.items():
        if isinstance(item, Mapping):
            yield from _walk(item, os.path.join(root, key))
        else:
            yield root, item

def _walk2(node, root = None):
    from collections.abc import Mapping
    if root is None:
        root = ''
    for key, item in node.items():
        if isinstance(item, Mapping):
            yield from _walk2(item, os.path.join(root, key))
        else:
            yield root, key, item

def _dirnames(node):
    from collections.abc import Mapping
    for key, item in node.items():
        if isinstance(item, Mapping):
            yield key

def _walk_filenames(node):
    for root, key, item in _walk2(node, root = os.sep):
        yield os.path.join(root, key)

def _del_entry(cache, fn):
    nodes = fn.split(os.sep)
    try:
        del _get_nested(cache, [KEY_LS] + nodes[:-1])[nodes[-1]]
    except KeyError:
        pass

def _update_cache(cache, root):
    nodes = root.split(os.sep)
    parent = _get_nested(cache, [KEY_LS] + nodes[:-1])
    if nodes[-1] in parent:
        return
    isfile = True
    for dirpath, dirnames, filenames in os.walk(root):
        isfile = False
        cache_ = _get_nested(cache, [KEY_LS] + dirpath.split(os.sep))
        for fn in filenames:
            fp = os.path.join(dirpath, fn)
            try:
                size = os.path.getsize(fp)
                cache_[fn] = size
            except (FileNotFoundError, OSError) as error:
                pass
    if isfile:
        parent[nodes[-1]] = os.path.getsize(root)

def _get_size_cached(cache, root, outdated = None):
    if outdated is None:
        outdated = set()
    for dirpath, dirnames, filenames in os.walk(root):
        logger.debug("Fetched {}".format(dirpath))
        cache_ = _get_nested(cache, [KEY_LS] + dirpath.split(os.sep))
        if cache_:
            outdated_ = set()
            for dirname in _dirnames(cache_):
                try:
                    dirnames.remove(dirname)
                except ValueError:
                    outdated_.add(dirname)
            for dp, fn, size in _walk2(cache_, dirpath):
                if dp in outdated_:
                    continue
                logger.debug('[CACHED]{}'.format(os.path.join(dp,fn)))
                if dp == dirpath:
                    try:
                        filenames.remove(fn)
                    except ValueError:
                        outdated.add(os.path.join(dp, fn))
                        continue
                yield os.path.join(dp, fn), size
            for dirname in outdated_:
                outdated.add(os.path.join(dirpath, dirname))
        for fn in filenames:
            fp = os.path.join(dirpath, fn)
            try:
                size = os.path.getsize(fp)
                logger.debug("Caching {}".format(fn))
                cache_[fn] = size
                yield fp, size
            except (FileNotFoundError, OSError) as error:
                logger.warning(error)
    for fn in outdated:
        _del_entry(cache, fn)

def clear_cache(cache, root):
    logger.info("Removing cache entry for '{}'.".format(root))
    _del_entry(cache, root)

def update_cache(cache, root, paths):
    if not KEY_LS in cache:
        return
    # To ensure that we recache flagged paths, even if the flag
    # was removed by a different process, we store the list of
    # flagged paths within the cache file.
    # The set of flagged paths is the union of the old set of
    # active paths and the new set of flagged paths.
    cached_flagged = set(cache.get(KEY_FLAGGED, list()))
    cache[KEY_FLAGGED] = list(paths)
    paths.update(cached_flagged)
    # Update all flagged paths
    for fn in paths:
        try:
            logger.info("Update cache for '{}'.".format(fn))
            _del_entry(cache, fn)
            _update_cache(cache, str(fn))
        except FileNotFoundError as error:
            pass

def update_cache_match(cache, root, match):
    if not KEY_LS in cache:
        return
    paths = set()
    for fn in _walk_filenames(cache[KEY_LS]):
        if match(fn):
            paths.add(fn)
    return update_cache(cache, root, paths)

def get_size(root = '.', cache = None):
    root = os.path.abspath(root)
    if cache is None:
        yield from _get_size(root=root)
    else:
        yield from _get_size_cached(cache=cache, root=root)
