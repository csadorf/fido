import sys
from setuptools import setup, find_packages
if sys.version_info[0] != 3:
    print("Error: fido requires python version >= 3.x.")
    sys.exit(1)

setup(
    name = 'fido',
    version = '0.2.2',
    license = 'MIT',
    package_dir = {'': 'src'},
    packages = find_packages('src'),

    author = 'Carl Simon Adorf',
    author_email = 'csadorf@umich.edu',
    description = "A light-weight cached disk usage monitoring tool.",
    keywords = 'monitoring disk usage cached',

    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Topic :: System :: Systems Administration",
        ],

    entry_points = {
        'console_scripts': [
            'duc  = fido.duc:main',
            'fido = fido.main:main',
            'fidomark = fido.mark:main',
        ]
    },
)
