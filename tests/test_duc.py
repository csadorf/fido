import unittest, os

def random_string(n):
    import random, string
    return ''.join(random.choice(string.ascii_uppercase) for i in range(n))

class DucTest(unittest.TestCase):
    
    def setUp(self):
        import tempfile
        self._tmp_dir = tempfile.TemporaryDirectory(prefix = 'fido_')

    def tearDown(self):
        self._tmp_dir.cleanup()

    def test_empty(self):
        from fido.duc import run_estimate
        with self._tmp_dir:
            s = run_estimate(self._tmp_dir.name)
            self.assertEqual(s, 0)

    def test_one_file(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            with open(os.path.join(tmp, 'test'), 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name)
            self.assertEqual(s, len(blob))

class CachedDucTest(unittest.TestCase):
    
    def setUp(self):
        import tempfile, os
        self._tmp_dir = tempfile.TemporaryDirectory(prefix = 'fido_')
        self._fn_cache = tempfile.NamedTemporaryFile(prefix = 'fido_test_cache')

    def tearDown(self):
        self._tmp_dir.cleanup()


class CachedDucTestBasic(CachedDucTest):

    def test_empty(self):
        from fido.duc import run_estimate
        with self._tmp_dir:
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)

    def test_one_file(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            with open(os.path.join(tmp, 'test'), 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))

    def test_insert_file(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)
            with open(os.path.join(tmp, 'test'), 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))

    def test_delete_file(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            fn_test = os.path.join(tmp, 'test')
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            os.remove(fn_test)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)

    def test_insert_dir(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)
            os.mkdir(os.path.join(tmp, 'test'))
            fn_test = os.path.join(tmp, 'test', 'test')
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))

    def test_remove_dir(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)
            os.mkdir(os.path.join(tmp, 'abc'))
            fn_test = os.path.join(tmp, 'abc', 'test')
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            os.remove(fn_test)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name, regex = 'abc')
            self.assertEqual(s, 0)

    def test_clear_cache(self):
        from fido.duc import run_estimate
        with self._tmp_dir as tmp:
            blob = random_string(20)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)
            os.mkdir(os.path.join(tmp, 'abc'))
            fn_test = os.path.join(tmp, 'abc', 'test')
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            os.remove(fn_test)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, len(blob))
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name, clear = True)
            self.assertEqual(s, 0)

class CachedDucTestFlagged(CachedDucTest):
    
    def test_insert_dir(self):
        from fido.duc import run_estimate
        from fido import mark
        with self._tmp_dir as tmp:
            blob = random_string(20)
            s = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s, 0)
            os.mkdir(os.path.join(tmp, 'abc'))
            mark(tmp, os.path.join(tmp, 'abc'))
            fn_test = os.path.join(tmp, 'abc', 'test')
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
            s0 = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
                fd.write(blob.encode())
            s1 = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            self.assertEqual(s1 - s0, len(blob))

    def test_competing_caches(self):
        import tempfile
        from fido.duc import run_estimate
        from fido import mark
        self._fn_cache_2 = tempfile.NamedTemporaryFile(prefix = 'fido_test_cache')
        with self._tmp_dir as tmp:
            blob = random_string(20)
            s0a = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            s0b = run_estimate(self._tmp_dir.name, self._fn_cache_2.name)
            self.assertEqual(s0a, 0) # sanity check
            self.assertEqual(s0b, 0) # sanity check
            os.mkdir(os.path.join(tmp, 'abc'))
            mark(tmp, os.path.join(tmp, 'abc'))
            fn_test = os.path.join(tmp, 'abc', 'test')
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
            s1a = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            s1b = run_estimate(self._tmp_dir.name, self._fn_cache_2.name)
            with open(fn_test, 'wb') as fd:
                fd.write(blob.encode())
                fd.write(blob.encode())
            mark(tmp, os.path.join(tmp, 'abc'), once=True)
            s2a = run_estimate(self._tmp_dir.name, self._fn_cache.name)
            # now the path is no longer flagged, but cache b 'remembers' the flag
            s2b = run_estimate(self._tmp_dir.name, self._fn_cache_2.name)
            self.assertEqual(s2a - s1a, len(blob))
            self.assertEqual(s2b - s1b, len(blob))

if __name__ == '__main__':
    unittest.main()
